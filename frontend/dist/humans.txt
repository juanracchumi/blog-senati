/* TEAM */  

  CEO: Aquiles Chulluncuy
  Email: aquiles@combativa.com
  Location: Lima, Lima, Perú
  
  CTO: Daniel Aguirre
  Email: daniel@combativa.com
  Location: Lima, Lima, Perú

  Frontend Developer: Juan López Jorge
  Twitter: @JuanLopezDev
  Personal Email: juanlopez.developer@gmail.com
  Company Email: juan@combativa.com
  Location: Lima, Lima, Perú.			
  
  Web Designer: Luis Ortiz
  Personal Email: luismar.ortiz@gmail.com 
  Company Email: luis@lanaranjamedia.com
  Location: Lima, Lima, Perú.

/* SITE */

  Site Name: Senda Inmobiliaria
  Last update: 2018/03/31
