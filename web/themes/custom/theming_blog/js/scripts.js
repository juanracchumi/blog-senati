/*
* Slider events
*/
$('#slider-events').slickPreload({
slidesToShow: 4,
slidesToScroll: 1,
slide: '.se-event',
margin: '15px',
responsive: [
    {
    breakpoint: 990,
    settings: {
        slidesToShow: 3
    }
    },
    {
    breakpoint: 767,
    settings: {
        slidesToShow: 2
    }
    },
    {
    breakpoint: 575,
    settings: {
        slidesToShow: 1
    }
    }
]
});